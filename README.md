# inChurch DevOps Recruitment

This challenge seeks to test your knowledge as Devops. The scope of this challenge is purposefully open and you are welcome to implement it in varying degrees of complexity.

### Requirements
- AWS account (if you don't have one, create a free tier account)

## Application

Create a Python API project that calculates the speed of an object, taking 2 parameters: the distance covered (in meters) and the time interval (in seconds).

### GET /api/speed

The endpoint accepts the following parameters

| Parameter | Description | Type |
|---|---|---|
| distance (reqeuired) | Distance traveled | int |
| time (required) | Time to travel the distance | int |


### Examples

- Request
```
GET http://127.0.0.1:8080/api/speed?distance=100&time=10
```

- Success Response

```
Status: 200
```
``` json
{
    ​"speed": "10.0 m/s"
}
```

- Error Response (use an appropriate error message)

```
Status: 400
```
``` json
{
    "error_message": "The parameter 'time' is required"
}
```

### Important!
You must write tests for your application


## CI/CD

The next step is to create a CI/CD pipeline for your application

- Create a Docker container for the project
- Deploy your application to an AWS server (using Infrastructure as Code is a plus).
- Create a CI/CD pipeline using the technologies of your choice so that any commit on the master that passes all tests must be deployed


## What Should I deliver?
- Fork this repository (Click on the '+' button on the left menu and then on 'Fork this repository')
- Commit the application as well as Docker related files and any other files related to infrastructure or CI/CD
- Set your AWS environment
- Send us a video recording your screen showing your CI/CD pipeline in action (in case you use IaC you can also record the environment being set)
- Describe in the README your technical choices justifying each one
- Describe in the README what you would like to have done if you had more time


## What is going to be evaluated?
- Code organization
- Technical choices: Justification for choosing libraries, architecture, etc.
- Security: Are there any vulnerabilities that have not been reported?
- Unit tests


## Do you have any questions? ##

Contact: rafael.reis@inchurch.com.br

# ------------------------------------ Resolução-------------------------------------------------------
## Link da aplicação: https://ec2.devopstests.com/api/speed?distance=100&time=10

## Flask
### Descrição
- Essa api foi desenvolvido utilizando flask pela simplicidade e rapidez que é possível criar um projeto em python para a camada rest, levando em consideração que não seria necessário
utilizar uma arquitetura muito rebuscada pelo contexto do projeto achei que seria a melhor escolha, caso tivesse que ter alguma interação com um banco de dados eu teria utilizado django rest.
- Foram desenvolvidos 2 endpoints com as respectivas validações e testes de cada endpoint e os fluxos de exceção.
### Como executar 
- Para executar a aplicação é necessário que tenha o python3 instalado e executar o seguintescommandos dentro do diretório `SpeedObjectAPI`:
```
pip3 install -r requirements.txt
python main.py
``` 
- Uma outra forma seria executar o `Dockerfile` utilizando o seguinte comando:
```
docker run -p 8080:8080 zero101010/velocityapi
```
## Terraform
- Foi utilizado uma configuração simples do terraform para criar uma máquina ec2 dentro da aws, onde eu defini as minhas credenciais utilizando `aws configure` e logo depois executei o Terraform com o comando
`terraform apply`.
 Depois da criação da máquina eu somente precisei pegar a chave de acesso para conseguir acessar pelo ssh tanto na minha máquina (para fazer as configurações do nginx) quanto no CI (para atualizar
a imagem quando uma nova versão fosse atualizada).
- A minha primeira escolha seria o Cloudformation da AWS(https://aws.amazon.com/pt/cloudformation/) no entano não gosto de deixar toda a minha infra em um lugar só pois isso pode gerar uma dependência da cloud, fazendo com que toda sua infra dependa de uma cloud específica, quando ela tem que ser agnóstica para poder ser moldada em qualquer ambiente e dando liberdade para que possa ser modificada caso seja necessário.

## CI/CD
- Utilizei o Gitlab-ci como ferramenta de CI/CD executando um mirror do repositório do bitbucket. Escolhi utilizar o Gitlab-ci or achar que seja uma das melhores que tem hoje no mercado quando comparado com azure Devops, Circle ci, jenkins e travis. Levando em consideração também que é desenvolvida em golang, que retorna uma performance muito boa.
- No CI dividi em basicamente 3 etapas o processo de CI/CD, uma chamada test(responsável por executar os testes do projeto), a segunda release(responsável por subir a imagem do docker buildada no ci para o registry) e a terceira chamada de deploy(responsável por acessar via ssh a máquina ec2 e atualizar a imagem que está lá na aws com a nova imagem gerada no stage release). 
Foi necessário criar algumas envs para gerenciar o meu CI/CI as adicionei nas variaveis do gitlab como mostra na imagem abaixo:

![alt text](https://i.imgur.com/CrqlJta.png) 

- Após toda a Criação do Ci e organização das envs o resultado foi esse:

![alt text](https://i.imgur.com/5hsEbuh.png) 


## Domínio + Nginx
- Para a configuração do domínio somente tive que pegar o ip da máquina no ec2 e adicionar no meu gerenciador de dns. Tive também que liberar as portas para acessar externamente.
- Para usar o https na api foi utilizado o nginx para fazer um proxy da aplicação que roda na porta 8080 do meu servidor para a porta 80. Após criar o proxy adicionei um certificado ssl para https funcionar e para isso utilizei o certbot para criar esse certificado, a configuração do arquivo de configuração do nginx está no arquivo `nginx.config`


## Melhorias 
### 1- Utilizar um K8S para a aplicação, levando em consideração a possível escalabilidade da aplicação, claro que isso dependeria do contexto da aplicação e do consumo de recursos. Na pasta K8S deixei os arquivos declarativos do próprio K8S para uma futura melhoria, no entanto não fiz o deploy do k8s também pois o free tier do ec2 exigiria que meu EKS tivesse no mínimo 5 nodes.
### 2- Utilizar o Helm para gerenciar os arquivos declarativos do k8s para facilitar o cotrole de envs
### 3- Usaria alguma ferramenta de mesh service, para gerenciar comunicação entre serviços e o gerenciamento de gateways de entrada e de proxy como o istio.
### 4 - Utilizaria o grafana ou prometeus para observar a saúde dos meus pods e clusters, no entanto usando o istio eu estaria usando um sidecar dentro de cada pod para colher essas métricas pelo prometeus e grafana, então não seria necessário instalar, mas caso não utilizasse o istio utilizaria eles a parte.
### 5 - Levando em consideração a aplicação, poderia ter sido feito usando serveless, usando lambda ou até mesmo o knative.
## Observações:
- Link do repositório do CI: https://gitlab.com/Zero101010/inChurchDevOps
- Link da imagem no DockerHub: https://hub.docker.com/r/zero101010/velocityapi

